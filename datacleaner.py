import pandas as pd
import re
import os
from tqdm import tqdm
from io import StringIO
from io import BytesIO
import argparse


class PathGrab():
    def __init__(self, file):
        self.file = file

    def get_path(self):
        ''' 
        This function automagically grabs the path to a file that you specify
        
        Arguments: 
            None

        Returns: 
            full path to file, as well as a list containing the path split at the '.' e.g. ['C:/Users/Whatever/Documents/file', 'csv']
        '''
        input_file = self.file
        module_path = os.path.dirname(os.path.realpath(__file__))

        input_path = os.path.join(module_path, input_file)

        decision = input_path.split('.')

        return input_path, decision


class CsvClean():
    def __init__(self, csv, chat_column_name, label_column_name_1, label_column_name_2, output_dir):
        self.csv = csv
        self.chat = chat_column_name
        self.label = label_column_name_1
        self.label2 = label_column_name_2
        self.output_dir = output_dir
      
    def dataloadc(self):
        '''
        This function cleans the data from a csv into a semi-usable format

        Arguments:
            csv object

        Returns:
            None
        
        Writes:
            chattest.txt (line delimited chat conversations)
        '''
        os.chdir(self.output_dir)
        file = open('chattest.txt', 'w', encoding='utf-8')
        file.close()
        with open('chattest.txt', 'r+', encoding='utf-8') as f:
            #loop = tqdm(total=len(csv['Transcript']), position=0) I wanted to see if tqdm might work for this.
            for i in range(len(self.csv[self.chat])):
                m = re.compile(r'\n')
                if type(self.csv[self.chat][i]) != float:
                    buf = StringIO(self.csv[self.chat][i])
                else:
                    buf = StringIO(' ')
                print(f'Chat {i} of {len(self.csv[self.chat])}')
                f.write((m.sub('', str(buf.readlines())))+'\n')
        file = open('labeltest.txt', 'w', encoding='utf-8')
        file.close()
        with open('labeltest.txt', 'r+', encoding='utf-8') as f:
            for i in range(len(self.csv[self.label])):
                m = re.compile(r'\n')
                if type(self.csv[self.label][i]) != float:
                    buf = StringIO(self.csv[self.label][i])
                else:
                    buf = StringIO('Neither')
                print(f'Label {i} of {len(self.csv[self.label])}')
                f.write((m.sub('', str(buf.readlines())))+'\n')
        file = open('labeltest2.txt', 'w', encoding='utf-8')
        file.close()
        with open('labeltest2.txt', 'r+', encoding='utf-8') as f:
            for i in range(len(self.csv[self.label2])):
                m = re.compile(r'\n')
                if type(self.csv[self.label2][i]) != float:
                    buf = StringIO(self.csv[self.label2][i])
                else:
                    buf = StringIO('Neither')
                print(f'Label {i} of {len(self.csv[self.label2])}')
                f.write((m.sub('', str(buf.readlines())))+'\n')



class XlClean():
    def __init__(self, xl, chat_column_name, label_column_name_1, label_column_name_2, output_dir):
        self.xl = xl
        self.chat = chat_column_name
        self.label = label_column_name_1
        self.label2 = label_column_name_2
        self.output_dir = output_dir

    def dataloadx(self):
        '''
        This function cleans the data from an Excel file into a semi-usable format

        Arguments:
            xl object

        Returns:
            None
        
        Writes:
            chattest.txt (line delimited chat conversations)
        '''
        os.chdir(self.output_dir)
        file = open('chattest.txt', 'w', encoding='utf-8')
        file.close()
        with open('chattest.txt', 'r+', encoding='utf-8') as f:
                for i in range(len(self.xl[self.chat])):
                    m = re.compile(r'\n')
                    if type(self.xl[self.chat][i]) == str:
                        buf = StringIO(self.xl[self.chat][i])
                    elif type(self.xl[self.chat][i]) == float:
                        buf = StringIO(' ')
                    else:
                        buf = StringIO(' ')
                    print(f'Chat {i} of {len(self.xl[self.chat])}')
                    f.write((m.sub('', str(buf.readlines())))+'\n')
        file = open('labeltest.txt', 'w', encoding='utf-8')
        file.close()
        with open('labeltest.txt', 'r+', encoding='utf-8') as f:
            for i in range(len(self.xl[self.label])):
                m = re.compile(r'\n')
                if type(self.xl[self.label][i]) == str:
                    buf = StringIO(self.xl[self.label][i])
                elif type(self.xl[self.label][i]) == float:
                    buf = StringIO('Neither')
                else:
                    buf = StringIO('Neither')
                print(f'Label {i} of {len(self.xl[self.label])}')
                f.write((m.sub('', str(buf.readlines())))+'\n')
        file = open('labeltest2.txt', 'w', encoding='utf-8')
        file.close()
        with open('labeltest2.txt', 'r+', encoding='utf-8') as f:
            for i in range(len(self.xl[self.label2])):
                m = re.compile(r'\n')
                if type(self.xl[self.label2][i]) == str:
                    buf = StringIO(self.xl[self.label2][i])
                elif type(self.xl[self.label2][i]) == float:
                    buf = StringIO(' ')
                else:
                    buf = BytesIO(self.xl[self.label2][i])
                print(f'Label {i} of {len(self.xl[self.label2])}')
                f.write((m.sub('', str(buf.readlines())))+'\n')
        '''
        This is preliminary testing for whether I could get the program to automatically detect whether a certain column was the right one. Not currently functional, so commented out.

        elif len(xl['text']) > 100:
            os.chdir(self.output_dir)
            with open('chattest.txt', 'r+', encoding='utf-8') as f:
                    for i in range(len(xl['text'])):
                        m = re.compile(r'\n')
                        if type(xl['text'][i]) == str:
                            buf = StringIO(xl['text'][i])
                        elif type(xl['text'][i]) == float:
                            buf = StringIO(' ')
                        else:
                            buf = StringIO(' ')
                        print(f'Row {i} of {len(xl["text"])}')
                        f.write((m.sub('', str(buf.readlines())))+'\n')'''

class Janitor():
    def __init__(self):
        pass

    def cleanup():
        '''
        This function cleans up the chattest.txt file and creates the chats.txt file, a version without brackets, superfluous quotes, and non-printing characters.

        Arguments:
            None
            chattest.txt must exist

        Returns:
            None
            
        Writes:
            chats.txt
        '''
        with open('chattest.txt', 'r+', encoding='utf-8') as f:
            with open('chats.txt', 'w', encoding='utf-8') as g:
                for line in f.readlines():
                    a = re.compile(r"(^\[)|(?!\b\'\b)(\b\'+|\'+\b)|(\b\"+|\"+\b)|(\\r\\n?)|(\\r)|(\\n)|(?!\b\,\s)(\,)|(\]$)")
                    g.write(a.sub('', str(line)))
        with open('labeltest.txt', 'r+', encoding='utf-8') as f:
            with open('labels.txt', 'w', encoding='utf-8') as g:
                for line in f.readlines():
                    a = re.compile(r"(^\[)|(?!\b\'\b)(\b\'+|\'+\b)|(\b\"+|\"+\b)|(\\r\\n?)|(\\r)|(\\n)|(?!\b\,\s)(\,)|(\]$)")
                    g.write(a.sub('', str(line)))
        with open('labeltest2.txt', 'r+', encoding='utf-8') as f:
            with open('labels2.txt', 'w', encoding='utf-8') as g:
                for line in f.readlines():
                    a = re.compile(r"(^\[)|(?!\b\'\b)(\b\'+|\'+\b)|(\b\"+|\"+\b)|(\\r\\n?)|(\\r)|(\\n)|(?!\b\,\s)(\,)|(\]$)")
                    g.write(a.sub('', str(line)))

def main():
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--file_name", default="2016 Chats.xlsx", type=str,
                        help="The name of the file containing the data, either .csv, .xlsx, or json format.")
    parser.add_argument("--output_dir", default="./output", type=str,
                        help="The output directory for where the file needs to end up.")
    parser.add_argument("--chat_column_name", default="Transcript", type=str,
                        help="This is the name of the column filled with Chats text.")
    parser.add_argument("--label_column_name_1", default="Satisfaction level", type=str,
                        help="This is the name of the first column that you want the model to predict.")
    parser.add_argument("--label_column_name_2", default="Student-to-student", type=str,
                        help="This is the name of the second column that you want the model to predict.")
    args = parser.parse_args()

    if args.output_dir is None:
        raise ValueError("You must have an output directory.")
    
    Grabber = PathGrab(args.file_name)
    input_path, decision = Grabber.get_path()
    if decision[-1] == 'csv':
        csv = pd.read_csv(args.file_name)
        Cleaner = CsvClean(csv, args.chat_column_name, args.label_column_name_1, args.label_column_name_2, args.output_dir)
        Cleaner.dataloadc()
    elif decision[-1] == 'xlsx':
        xl = pd.read_excel(args.file_name)
        Cleaner = XlClean(xl, args.chat_column_name, args.label_column_name_1, args.label_column_name_2, args.output_dir)
        Cleaner.dataloadx()
    Janitor.cleanup()
                         
if __name__ == "__main__":
    main()
