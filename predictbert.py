import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from pathlib import Path 
from io import StringIO
from io import BytesIO
import argparse

from IPython.display import HTML

import os

import torch
import torch.optim as optim

import random 

# fastai
from fastai import *
from fastai.text import *
from fastai.callbacks import *
from transformers import AdamW
from functools import partial

# transformers
from transformers import PreTrainedModel, PreTrainedTokenizer, PretrainedConfig

from transformers import BertForSequenceClassification, BertTokenizer, BertConfig
from transformers import RobertaForSequenceClassification, RobertaTokenizer, RobertaConfig
from transformers import XLNetForSequenceClassification, XLNetTokenizer, XLNetConfig
from transformers import XLMForSequenceClassification, XLMTokenizer, XLMConfig
from transformers import DistilBertForSequenceClassification, DistilBertTokenizer, DistilBertConfig

import fastai
import transformers

import fastaibert as fb

class TransformersBaseTokenizer(BaseTokenizer):
    '''Wrapper around PreTrainedTokenizer

        This is just for it to be compatible with fast.ai
    '''
    def __init__(self, pretrained_tokenizer: PreTrainedTokenizer, model_type = 'bert', **kwargs):
        self._pretrained_tokenizer = pretrained_tokenizer
        self.max_seq_len = pretrained_tokenizer.max_len
        self.model_type = model_type

    def __call__(self, *args, **kwargs):
        return self

    def tokenizer(self, t:str) -> List[str]:
        '''Limits the maximum sequence length and adds the special tokens'''
        CLS = self._pretrained_tokenizer.cls_token
        SEP = self._pretrained_tokenizer.sep_token
        if self.model_type in ['roberta']:
            tokens = self._pretrained_tokenizer.tokenize(t, add_prefix_space=True)[:self.max_seq_len - 2]
        else:
            tokens = self._pretrained_tokenizer.tokenize(t)[:self.max_seq_len - 2]
        return [CLS] + tokens + [SEP]

class TransformersVocab(Vocab):
    def __init__(self, tokenizer: PreTrainedTokenizer):
        super(TransformersVocab, self).__init__(itos = [])
        self.tokenizer = tokenizer

    def numericalize(self, t:Collection[str]) -> List[int]:
        '''Converts a list of tokens 't' to their ids.'''
        return self.tokenizer.convert_tokens_to_ids(t)
        #return self.tokenizer.encode(t)

    def textify(self, nums:Collection[int], sep=' ') -> List[str]:
        '''Converts a list of 'nums' to their tokens.'''
        nums = np.array(nums).tolist()
        return sep.join(self.tokenizer.convert_ids_to_tokens(nums)) if sep is not None else self.tokenizer.convert_ids_to_tokens(nums)

    def __getstate__(self):
        return {'itos':self.itos, 'tokenizer':self.tokenizer}

    def __setstate__(self, state:dict):
        self.itos = state['itos']
        self.tokenizer = state['tokenizer']
        self.stoi = collections.defaultdict(int, {v:k for k,v in enumerate(self.itos)})

class CustomTransformerModel(nn.Module):
    def __init__(self, transformer_model: PreTrainedModel):
        super(CustomTransformerModel,self).__init__()
        self.transformer = transformer_model

    def forward(self, input_ids, attention_mask=None):
        #attention_mask = (input_ids!=1).type(input_ids.type()) #Test attention_mask for RoBERTa

        logits = self.transformer(input_ids,
                                  attention_mask = attention_mask)[0]
                                  
        return logits

class PathGrab():
    def __init__(self, file):
        self.file = file

    def get_path(self):
        ''' 
        This function automagically grabs the path to a file that you specify
        
        Arguments: 
            None

        Returns: 
            full path to file, as well as a list containing the path split at the '.' e.g. ['C:/Users/Whatever/Documents/file', 'csv']
        '''
        input_file = self.file
        module_path = os.path.dirname(os.path.realpath(__file__))

        input_path = os.path.join(module_path, input_file)

        decision = input_path.split('.')

        return input_path, decision

class CsvClean():
    def __init__(self, csv, chat_column_name, output_dir):
        self.csv = csv
        self.chat = chat_column_name
        self.output = output_dir
      
    def dataloadc(self):
        '''
        This function cleans the data from a csv into a semi-usable format

        Arguments:
            csv object

        Returns:
            None
        
        Writes:
            chattest.txt (line delimited chat conversations)
        '''
        os.chdir(self.output)
        file = open('chattest.txt', 'w', encoding='utf-8')
        file.close()
        with open('chattest.txt', 'r+', encoding='utf-8') as f:
            #loop = tqdm(total=len(csv['Transcript']), position=0) I wanted to see if tqdm might work for this.
            for i in range(len(self.csv[self.chat])):
                m = re.compile(r'\n')
                if type(self.csv[self.chat][i]) != float:
                    buf = StringIO(self.csv[self.chat][i])
                else:
                    buf = StringIO(' ')
                print(f'Chat {i} of {len(self.csv[self.chat])}')
                f.write((m.sub('', str(buf.readlines())))+'\n')

class XlClean():
    def __init__(self, xl, chat_column_name, output_dir):
        self.xl = xl
        self.chat = chat_column_name
        self.output = output_dir

    def dataloadx(self):
        '''
        This function cleans the data from an Excel file into a semi-usable format

        Arguments:
            xl object

        Returns:
            None
        
        Writes:
            chattest.txt (line delimited chat conversations)
        '''
        os.chdir(self.output)
        file = open('chattest.txt', 'w', encoding='utf-8')
        file.close()
        with open('chattest.txt', 'r+', encoding='utf-8') as f:
                for i in range(len(self.xl[self.chat])):
                    m = re.compile(r'\n')
                    if type(self.xl[self.chat][i]) == str:
                        buf = StringIO(self.xl[self.chat][i])
                    elif type(self.xl[self.chat][i]) == float:
                        buf = StringIO(' ')
                    else:
                        buf = StringIO(' ')
                    print(f'Chat {i} of {len(self.xl[self.chat])}')
                    f.write((m.sub('', str(buf.readlines())))+'\n')

class Janitor():
    def __init__(self):
        pass

    def cleanup():
        '''
        This function cleans up the chattest.txt file and creates the chats.txt file, a version without brackets, superfluous quotes, and non-printing characters.

        Arguments:
            None
            chattest.txt must exist

        Returns:
            None
            
        Writes:
            chats.txt
        '''
        with open('chattest.txt', 'r+', encoding='utf-8') as f:
            with open('chats.txt', 'w', encoding='utf-8') as g:
                for line in f.readlines():
                    a = re.compile(r"(^\[)|(?!\b\'\b)(\b\'+|\'+\b)|(\b\"+|\"+\b)|(\\r\\n?)|(\\r)|(\\n)|(?!\b\,\s)(\,)|(\]$)")
                    g.write(a.sub('', str(line)))

def load_data():
    Grabber = PathGrab(args.file_name)
    input_path, decision = Grabber.get_path()
    if decision[-1] == 'csv':
        csv = pd.read_csv(args.file_name)
        Cleaner = CsvClean(csv, args.chat_column_name, args.output_dir)
        Cleaner.dataloadc()
    elif decision[-1] == 'xlsx':
        xl = pd.read_excel(args.file_name)
        Cleaner = XlClean(xl, args.chat_column_name, args.output_dir)
        Cleaner.dataloadx()
    Janitor.cleanup()

    chatfile = open('chats.txt', 'r', encoding='utf-8')
    chats = list(map(lambda x:x[:-1], chatfile.readlines()))
    chatfile.close()
    os.chdir('..')

    return chats



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--file_name", default="librarychats.csv", type=str,
                        help="The name of the file containing the data, either .csv, .xlsx, or json format.")
    parser.add_argument("--chat_column_name", default="text", type=str,
                        help="This is the name of the column filled with Chats text.")
    parser.add_argument("--output_dir", default="./output", type=str,
                        help="The output directory for where the file needs to end up.")
    args = parser.parse_args()

    if args.chat_column_name is None:
        raise ValueError("You need to tell us which column contains the chats, otherwise this won't work.")

    chats = load_data()
    
    model = load_learner('.', 'transfer.pkl')

    file = open('preds.txt', 'w')
    file.close

    os.chdir('./output')
    with open('preds.txt', 'w', encoding='utf-8') as f:
        for chat in chats:
            pred = model.predict(chat)
            f.write(str(pred) + "\n")