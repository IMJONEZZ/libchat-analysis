import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from pathlib import Path 

from IPython.display import HTML

import os

import torch
import torch.optim as optim

import random 

# fastai
from fastai import *
from fastai.text import *
from fastai.callbacks import *
from transformers import AdamW
from functools import partial

# transformers
from transformers import PreTrainedModel, PreTrainedTokenizer, PretrainedConfig

from transformers import BertForSequenceClassification, BertTokenizer, BertConfig
from transformers import RobertaForSequenceClassification, RobertaTokenizer, RobertaConfig
from transformers import XLNetForSequenceClassification, XLNetTokenizer, XLNetConfig
from transformers import XLMForSequenceClassification, XLMTokenizer, XLMConfig
from transformers import DistilBertForSequenceClassification, DistilBertTokenizer, DistilBertConfig

import fastai
import transformers

def seed_all(seed_value):
    random.seed(seed_value)
    np.random.seed(seed_value)
    torch.manual_seed(seed_value)

    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False


class TransformersBaseTokenizer(BaseTokenizer):
    '''
    Wrapper around PreTrainedTokenizer
    This is just for it to be compatible with fast.ai

    Arguments:
        PreTrainedTokenizer - From transformers
        model_type - From transformers
        **kwargs

    Returns:
        CLS tokens [list]
        tokens
        SEP tokens [list]
    '''
    def __init__(self, pretrained_tokenizer: PreTrainedTokenizer, model_type = model_type, **kwargs):
        self._pretrained_tokenizer = pretrained_tokenizer
        self.max_seq_len = pretrained_tokenizer.max_len
        self.model_type = model_type

    def __call__(self, *args, **kwargs):
        return self

    def tokenizer(self, t:str) -> List[str]:
        '''Limits the maximum sequence length and adds the special tokens'''
        CLS = self._pretrained_tokenizer.cls_token
        SEP = self._pretrained_tokenizer.sep_token
        if self.model_type in ['roberta']:
            tokens = self._pretrained_tokenizer.tokenize(t, add_prefix_space=True)[:self.max_seq_len - 2]
        else:
            tokens = self._pretrained_tokenizer.tokenize(t)[:self.max_seq_len - 2]
        return [CLS] + tokens + [SEP]

class TransformersVocab(Vocab):
    '''
        Builds a vocabulary for the transformer model (DistilBert for us)

        Arguments:
            PreTrainedTokenizer - From transformers

        Returns:
            converted list of tokens into ids
            converted list of ids into tokens
            state of tokenizer
    '''
    def __init__(self, tokenizer: PreTrainedTokenizer):
        super(TransformersVocab, self).__init__(itos = [])
        self.tokenizer = tokenizer

    def numericalize(self, t:Collection[str]) -> List[int]:
        '''Converts a list of tokens 't' to their ids.'''
        return self.tokenizer.convert_tokens_to_ids(t)
        #return self.tokenizer.encode(t)

    def textify(self, nums:Collection[int], sep=' ') -> List[str]:
        '''Converts a list of 'nums' to their tokens.'''
        nums = np.array(nums).tolist()
        return sep.join(self.tokenizer.convert_ids_to_tokens(nums)) if sep is not None else self.tokenizer.convert_ids_to_tokens(nums)

    def __getstate__(self):
        return {'itos':self.itos, 'tokenizer':self.tokenizer}

    def __setstate__(self, state:dict):
        self.itos = state['itos']
        self.tokenizer = state['tokenizer']
        self.stoi = collections.defaultdict(int, {v:k for k,v in enumerate(self.itos)})

class CustomTransformerModel(nn.Module):
    '''
        A Custom Version of the transformer models that allows us to turn attention mask on or off (on only for bert and roberta)

        Arguments:
            PreTrainedModel - from transformers

        Returns:
            logits
    '''
    def __init__(self, transformer_model: PreTrainedModel):
        super(CustomTransformerModel,self).__init__()
        self.transformer = transformer_model

    def forward(self, input_ids, attention_mask=None):
        #attention_mask = (input_ids!=1).type(input_ids.type()) #Test attention_mask for RoBERTa

        logits = self.transformer(input_ids,
                                  attention_mask = attention_mask)[0]
        return logits

def get_preds_as_nparray(ds_type) -> np.ndarray:
    """
    the get_preds method does not return the elements in order by default
    I've borrowed the code from the RNNLearner to resort the elements into their correct order

    Arguments:
        ds_type

    Returns:
        predictions
    """
    preds = learner.get_preds(ds_type)[0].detach().cpu().numpy()
    sampler = [i for i in databunch.dl(ds_type).sampler]
    reverse_sampler = np.argsort(sampler)
    return preds[reverse_sampler, :]

def create_download_link(title = "Download CSV file", filename = "data.csv"): 
    '''
        This function is irrelevant outside of a website, but could help as part of a REST API.

        Arguments:
            Title
            Filename

        Returns:
            an HTML download link
    ''' 
    html = '<a href={filename}>{title}</a>'
    html = html.format(title=title,filename=filename)
    return HTML(html)


if __name__ == '__main__':
    DATA_ROOT = Path(".")
    train = pd.read_csv('train.tsv', sep='\t')
    test = pd.read_csv('test.tsv', sep='\t')

    #print(train.head())
    # This is just listing out all of the possible models available in the transformers module.
    MODEL_CLASSES = {
        'bert': (BertForSequenceClassification, BertTokenizer, BertConfig),
        'xlnet': (XLNetForSequenceClassification, XLNetTokenizer, XLNetConfig),
        'xlm': (XLMForSequenceClassification, XLMTokenizer, XLMConfig),
        'roberta': (RobertaForSequenceClassification, RobertaTokenizer, RobertaConfig),
        'distilbert': (DistilBertForSequenceClassification, DistilBertTokenizer, DistilBertConfig)
        }

    seed = 42
    use_fp16 = False
    bs = 16
    
    #model_type = 'roberta'
    #pretrained_model_name = 'roberta-base'

    #model_type = 'bert'
    #pretrained_model_name='bert-base-uncased'

    model_type = 'distilbert'
    pretrained_model_name = 'distilbert-base-uncased'

    #model_type = 'xlm'
    #pretrained_model_name = 'xlm-clm-enfr-1024'

    #model_type = 'xlnet'
    #pretrained_model_name = 'xlnet-base-cased'

    model_class, tokenizer_class, config_class = MODEL_CLASSES[model_type]

    #print(model_class.pretrained_model_archive_map.keys())

    seed_all(seed)

    # Setting up the tokenizer to take our inputs.
    transformer_tokenizer = tokenizer_class.from_pretrained(pretrained_model_name)
    transformer_base_tokenizer = TransformersBaseTokenizer(pretrained_tokenizer = transformer_tokenizer, model_type = model_type)
    fastai_tokenizer = Tokenizer(tok_func = transformer_base_tokenizer, pre_rules=[], post_rules=[])

    transformer_vocab = TransformersVocab(tokenizer = transformer_tokenizer)
    numericalize_processor = NumericalizeProcessor(vocab=transformer_vocab)

    tokenize_processor = TokenizeProcessor(tokenizer=fastai_tokenizer, include_bos=False, include_eos=False)

    transformer_processor = [tokenize_processor, numericalize_processor]

    # Will be true if running on xlnet
    pad_first = bool(model_type in ['xlnet'])
    pad_idx = transformer_tokenizer.pad_token_id

    # test tokenizer
    tokens = transformer_tokenizer.tokenize('Salut c est moi, Hello it s me')
    #print(tokens)
    ids = transformer_tokenizer.convert_tokens_to_ids(tokens)
    #print(ids)
    transformer_tokenizer.convert_ids_to_tokens(ids)

    # This utilizes the Datablock API from fastai to combine essentially a Dataset and Dataloader class in Pytorch.
    databunch = (TextList.from_df(train, cols='Phrase', processor=transformer_processor)
                .split_by_rand_pct(0.1,seed=seed)
                .label_from_df(cols= 'Sentiment')
                .add_test(test)
                .databunch(bs=bs, pad_first=pad_first, pad_idx=pad_idx))

    #print('[CLS] token :', transformer_tokenizer.cls_token)
    #print('[SEP] token :', transformer_tokenizer.sep_token)
    #print('[PAD] token :', transformer_tokenizer.pad_token)
    databunch.show_batch()

    #print('[CLS] id :', transformer_tokenizer.cls_token_id)
    #print('[SEP] id :', transformer_tokenizer.sep_token_id)
    #print('[PAD] id :', pad_idx)
    test_one_batch = databunch.one_batch()[0]
    #print('Batch shape : ', test_one_batch.shape)
    #print(test_one_batch)

    #Set up the pretrained config for our pretrained model
    config = config_class.from_pretrained(pretrained_model_name)
    config.num_labels = 5 #super important!!!
    config.use_bfloat16 = use_fp16
    #print(config)

    transformer_model = model_class.from_pretrained(pretrained_model_name, config = config)
    #transformer_model = model_class.from_pretrained(pretrained_model_name, num_labels = 5)

    custom_transformer_model = CustomTransformerModel(transformer_model = transformer_model)

    CustomAdamW = partial(AdamW, correct_bias=False)

    # Stuff the model and the Data together with the optimizer
    learner = Learner(databunch,
                    custom_transformer_model,
                    opt_func = CustomAdamW,
                    metrics=[accuracy, error_rate])

    #Show graph of learner stats and metrics after each epoch.
    learner.callbacks.append(ShowGraph(learner))

    #Put learn in FP16 precision mode. --> Doesn't work, don't do it.
    if use_fp16: learner = learner.to_fp16()

    #print(learner.model)


    # For DistilBERT
    list_layers = [learner.model.transformer.distilbert.embeddings,
                   learner.model.transformer.distilbert.transformer.layer[0],
                   learner.model.transformer.distilbert.transformer.layer[1],
                   learner.model.transformer.distilbert.transformer.layer[2],
                   learner.model.transformer.distilbert.transformer.layer[3],
                   learner.model.transformer.distilbert.transformer.layer[4],
                   learner.model.transformer.distilbert.transformer.layer[5],
                   learner.model.transformer.pre_classifier]

    # For roberta-base
    #list_layers = [learner.model.transformer.roberta.embeddings,
    #               learner.model.transformer.roberta.encoder.layer[0],
    #               learner.model.transformer.roberta.encoder.layer[1],
    #               learner.model.transformer.roberta.encoder.layer[2],
    #               learner.model.transformer.roberta.encoder.layer[3],
    #               learner.model.transformer.roberta.encoder.layer[4],
    #               learner.model.transformer.roberta.encoder.layer[5],
    #               learner.model.transformer.roberta.encoder.layer[6],
    #               learner.model.transformer.roberta.encoder.layer[7],
    #               learner.model.transformer.roberta.encoder.layer[8],
    #               learner.model.transformer.roberta.encoder.layer[9],
    #               learner.model.transformer.roberta.encoder.layer[10],
    #               learner.model.transformer.roberta.encoder.layer[11],
    #               learner.model.transformer.roberta.pooler]

    learner.split(list_layers)
    num_groups = len(learner.layer_groups)
    print('Learner split in',num_groups,'groups')
    print(learner.layer_groups)

    learner.save('untrain')

    seed_all(seed)
    learner.load('untrain')

    learner.freeze_to(-1)

    learner.summary()

    learner.lr_find()

    learner.recorder.plot(skip_end=10, suggestion=True)

    learner.fit_one_cycle(1,max_lr=2e-03,moms=(0.8,0.7))

    learner.save('first_cycle')

    seed_all(seed)
    learner.load('first_cycle')

    learner.freeze_to(-2)

    lr = 1e-5

    learner.fit_one_cycle(1, max_lr=slice(lr*0.95**num_groups, lr), moms=(0.8,0.9))

    learner.save('second_cycle')

    seed_all(seed)
    learner.load('second_cycle')

    learner.freeze_to(-3)

    learner.fit_one_cycle(1, max_lr=slice(lr*0.95**num_groups, lr), moms=(0.8,0.9))

    learner.save('third_cycle')

    seed_all(seed)
    learner.load('third_cycle')

    learner.unfreeze()

    learner.fit_one_cycle(3, max_lr=slice(lr*0.95**num_groups, lr), moms=(0.8,0.9))

    pred = learner.predict('This is the best movie of 2020')
    print(pred)

    pred = learner.predict('This is the worst movie of 2020')
    print(pred)

    learner.export(file = 'transformer1.pkl')

    test_preds = get_preds_as_nparray(DatasetType.Test)

    print(test_preds)

    #sample_submission = pd.read_csv(DATA_ROOT / 'sampleSubmission.csv')
    #sample_submission['Sentiment'] = np.argmax(test_preds,axis=1)
    #sample_submission.to_csv("predictions.csv", index=False)


